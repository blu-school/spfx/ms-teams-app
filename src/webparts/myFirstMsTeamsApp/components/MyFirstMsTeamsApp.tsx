import * as React from 'react';
import styles from './MyFirstMsTeamsApp.module.scss';
import { IMyFirstMsTeamsAppProps } from './IMyFirstMsTeamsAppProps';

export default class MyFirstMsTeamsApp extends React.Component<IMyFirstMsTeamsAppProps, {}> {
  public render(): React.ReactElement<IMyFirstMsTeamsAppProps> {
    return (
      <div className={styles.myFirstMsTeamsApp}>
        <div className={styles.container}>
          <div className={styles.row}>
            <img src="https://memegenerator.net/img/instances/400x/65029497.jpg" />
            <div>
              And build some awesome Microsoft Teams Apps!
            </div>
          </div>
        </div>
      </div>
    );
  }
}
