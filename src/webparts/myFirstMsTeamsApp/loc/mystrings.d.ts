declare interface IMyFirstMsTeamsAppWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'MyFirstMsTeamsAppWebPartStrings' {
  const strings: IMyFirstMsTeamsAppWebPartStrings;
  export = strings;
}
