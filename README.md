# ms-teams-app

# Install dependencdies
`npm install`

# Run the project
`gulp serve`

# Deployment

## Create a package solution
 1. `gulp serve`
 2. `gulp trust-dev-cert`
 3. `gulp bundle`
 4. `gulp package-solution`

## Get a free Teams developer tenant

   1. Go to the [Microsoft 365 developer program](https://developer.microsoft.com/en-us/microsoft-365/dev-program).
   2. Select Join Now and log in with your blubito account
   3. In the welcome screen, select Set up E5 subscription.
   4. Set up your administrator account. 
   5. Install [Users & Sharepoint Samples Data Packs](https://docs.microsoft.com/en-us/office/developer-program/install-sample-packs)
   6. After you finish, you should see a screen like this.
   
   ![alt text](images/DeveloperTenant.PNG "Developer Tenant")

   5. Click on Go to subscription
   6. Click on the MS Teams icon in the navigation bar on the left side

## Deploy to the Tenant App Catalog
   1. Navigate to Tenant App Catalog. If you do not have Tenant App Catalog, follow the [link](https://docs.microsoft.com/en-us/sharepoint/use-app-catalog) to create one 

   2. Click on the Upload button, navigate to the project folder & select the generated .sppkg package from sharepoint/solution folder
![alt text](images/UploadSolution.PNG "Sync to teams")

   3. Deploy the solution
![alt text](images/DeploySolution.PNG "Sync to teams")

   4. Sync the solution to Microsof Teams
![alt text](images/SyncToTeams.PNG "Sync to teams")

   5. Create a new channel & install the solution as a tab 

   6. Navigate to VS Code and run `gulp serve`

## Result

![alt text](images/Project.PNG "Title Text")

